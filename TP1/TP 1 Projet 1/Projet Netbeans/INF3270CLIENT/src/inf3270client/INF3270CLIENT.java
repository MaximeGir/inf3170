/**
 *
 * @author Maxime Girard
 * @INF3270 TÉLÉINFORMATIQUE
 * @Class INF3270CLIENT
 *
 * Ce petit programme utilise un cable de type "RS-232 NULL MODEM" pour se
 * connecter à une machine locale et procéder à un échange de données.
 *
 * Cette version du programme est la version CLIENT, c'est-à-dire que c'est ce
 * programme qui reçoit les données du programme SERVEUR, placé sur la deuxième
 * machine.
 *
 * Le programme doit identifier le port qu'il utilisera pour effectuer les
 * transactions avec le serveur, qui correspond évidement au port auquel le fil
 * est branché. Dans ce cas-ci j'ai arbitrairement décidé de vérifier tout les
 * ports susceptible d'être utilisé par une machine qui est équipé du
 * périphérique sériel ET des machines qui utiliseraient un adapteur USB ; Dans
 * ce cas précis, le numéro de port varie et peut aller jusqu'à "COM4".
 *
 * Lors de l'identification du port sériel qu'il va utiliser , le programme
 * boucle sur "COM1", "COM2","COM3" et "COM4", ce qui évite des manipulations
 * inutiles du code.
 * 
 * IMPORTANT : Il y a deux facon de fermer ce programme : 
 *  
 *             1) Vous arrêtez le processus dans l'IDE
 *             ou sur la ligne de commande, bref, l'arrête dois être manuel. 
 * 
 *             2) Vous envoyez le message "sortie" à partir du programme SERVEUR
 *                Ce qui aura comme effet d'arrêter les deux programmes en générant 
 *                un simple message de remerciement. 
 * 
 */

package inf3270client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.comm.*;
import java.util.*;

public class INF3270CLIENT {

    static int BAUD_RATE = 9600;
    static String commonPort = "COM1";
    static String otherPort = "COM2";
    static String desiredPort = "COM3";
    static String backUpPort = "COM4";
    static String applicationOwner = "INF3270";
    static int currentTimeOut = 5000;
    static CommPortIdentifier usingThisPort;
    static Enumeration portIdentifiers = CommPortIdentifier.getPortIdentifiers();

    /**
     * Méthode pour trouvé le bon port série.
     */
    public static CommPortIdentifier getProperSerialPort(Enumeration portIdentifiers) throws NoSuchPortException {

        CommPortIdentifier currentIdentifier;

        while (portIdentifiers.hasMoreElements()) {
            currentIdentifier = (CommPortIdentifier) portIdentifiers.nextElement();

            if (currentIdentifier.getPortType() == CommPortIdentifier.PORT_SERIAL
                    && (currentIdentifier.getName().equals(desiredPort))
                    || (currentIdentifier.getName().equals(backUpPort))
                    || (currentIdentifier.getName().equals(commonPort))
                    || (currentIdentifier.getName().equals(otherPort))) {

                usingThisPort = currentIdentifier;

                System.out.println("::::Identification du port::::");
                System.out.println("Port série trouvé : " + usingThisPort.getName());

                return usingThisPort;
            } else {
                usingThisPort = null;
            }
        }
        return usingThisPort;
    }

    /**
     * Méthode pour aquérir les permissions reliées au port série.
     */
    public static SerialPort takePortOwnership(CommPortIdentifier workingPort) throws PortInUseException, NullPointerException {

        SerialPort useablePort = (SerialPort) workingPort.open(applicationOwner, currentTimeOut);
        System.out.println("::::Acquisition du port::::");
        return useablePort;
    }

    /**
     * Méthode pour configurer le port serie Prends en parametre le SerialPort à
     * configurer Retourne le SerialPort configuré.
     */
    
    public static SerialPort configureCurrentSerialPort(SerialPort useablePort)
            throws UnsupportedCommOperationException {

        useablePort.setSerialPortParams(BAUD_RATE,
                SerialPort.DATABITS_8,
                SerialPort.STOPBITS_2,
                SerialPort.PARITY_EVEN);
        System.out.println("::::Configuration du port::::");

        return useablePort;
    }

    /**
     * Méthode qui configure le port pour la réception de caractères.
     */
    
    public static BufferedReader inputStreamSetup(SerialPort configuredSerialPort) throws IOException {
        BufferedReader input;
        input = new BufferedReader(new InputStreamReader(configuredSerialPort.getInputStream(),"UTF-8"));

        System.out.println("::::Acquisition du canal de communication::::");
        return input;
    }

    /*
     * Méthode qui récupère le message de l'utilisateur taper au clavier. 
     * Le programme quitte si l'utilisateur tape le mot 'sortie'.
     **/
    
    public static String writeMessage() {
        System.out.print("Entrez le message a envoyer (Entrez 'sortie' pour quitter): ");
        Scanner scan = new Scanner(System.in);
        String messageToSend = scan.nextLine();

        if ("sortie".equals(messageToSend)) {
            System.out.println("Merci , au revoir.");
            System.exit(0);
        }
        return messageToSend;
    }

    /**
     * Programme principal
     */
    
    public static void main(String[] args) {

        boolean connectionIsAlive = true;

        try {

            CommPortIdentifier workingPort = getProperSerialPort(portIdentifiers);
            SerialPort useablePort = takePortOwnership(workingPort);
            SerialPort configuredSerialPort = configureCurrentSerialPort(useablePort);
            BufferedReader inputReader = inputStreamSetup(configuredSerialPort);

            /**
             * TRANSACTIONS (unilatérale, dans ce cas)
             */
            System.out.println("::::Status : Prêt, En attente::::");

            while (connectionIsAlive) {

                String incomingMessage = inputReader.readLine();

                if (incomingMessage.length() != 0) {
                    System.out.println("Le message reçu est : " + incomingMessage + "\n");

                    if ("sortie".equals(incomingMessage)) {

                        System.out.println("Le serveur s'est déconnecté, ce programme va se fermer.");
                        System.out.println("Merci, au revoir");
                        System.exit(0);
                    }
                } else {
                    incomingMessage = null;
                    System.gc();
                    Thread.sleep(100);
                }
            }

        } catch (NoSuchPortException e) {
            System.err.println("Une erreur est survenue lors de la détection des ports, veuillez réessayez.");
        } catch (PortInUseException e) {
            System.err.println("Le port ne peut garantir l'exclusivité à ce programme, veuillez relancer le programme.");
        } catch (UnsupportedCommOperationException UCOE) {
            System.err.println("Le port ne peut être configurer pour le moment, veuillez relancer le programme.");
        } catch (IOException IOE) {
            System.err.printf("Erreur lors de l'élaboration du protocole, veuillez relancer le programme.");
        } catch (InterruptedException IPE) {
            System.err.println("Un problème de processus est survenue, veuillez relancer le programme.");
        } catch (NullPointerException NPE) {
            System.err.println("Verifiez que le cable soit bien branché et relancez le programme.");
        }
    }
}