/**
 * @Author Maxime Girard
 * @Author INF3270 AUTOMNE 2012
 * @Class INF3270SERVEUR
 * 
 * Ce petit programme utilise un cable de type "RS-232 NULL-MODEM" pour se 
 * connecter à une machine locale et procéder à un échange de données.
 * 
 * Ce programme est un logiciel en deux version. Cette version du programme
 * est la version SERVEUR c'est-à-dire que c'est ce programme qui ENVOIE les données
 * dans la version CLIENT placé sur la deuxième machine.
 * 
* Le programme doit identifier le port qu'il utilisera pour effectuer les
 * transactions avec le serveur, qui correspond évidement au port auquel le fil
 * est branché. Dans ce cas-ci j'ai arbitrairement décidé de vérifier tout les
 * ports susceptible d'être utilisé par une machine qui est équipé du
 * périphérique sériel ET des machines qui utiliseraient un adapteur USB ; Dans
 * ce cas précis, le numéro de port varie et peut aller jusqu'à "COM4".
 *
 * Lors de l'identification du port sériel qu'il va utiliser , le programme
 * boucle sur "COM1", "COM2","COM3" et "COM4", ce qui évite des manipulations
 * inutiles du code.
 * 
 * IMPORTANT : Pour quitter ce programme vous devez entrez le mot "sortie" dans 
 *             console, ce qui aura pour effet d'arrêter le processus du présent
 *             programme ET du programme CLIENT.
 * 
 */
package inf3270serveur;

import java.io.IOException;
import java.io.PrintStream;
import javax.comm.*;
import java.util.*;

public class INF3270SERVEUR {

    static int BAUD_RATE = 9600;
    static String commonPort = "COM1";
    static String otherPort = "COM2";
    static String desiredPort = "COM3";
    static String backUpPort = "COM4";
    static String applicationOwner = "INF3270";
    static int currentTimeOut = 5000;
    static CommPortIdentifier usingThisPort;
    static Enumeration portIdentifiers = CommPortIdentifier.getPortIdentifiers();

    /**
     * Méthode pour trouver le bon port série.
     */
    public static CommPortIdentifier getProperSerialPort(Enumeration portIdentifiers) throws NoSuchPortException , NullPointerException {

        CommPortIdentifier currentIdentifier;

        while (portIdentifiers.hasMoreElements()) {

            currentIdentifier = (CommPortIdentifier) portIdentifiers.nextElement();

            if (currentIdentifier.getPortType() == CommPortIdentifier.PORT_SERIAL
                    && ((currentIdentifier.getName().equals(desiredPort))
                    || (currentIdentifier.getName().equals(commonPort))
                    || (currentIdentifier.getName().equals(otherPort))
                    || (currentIdentifier.getName().equals(backUpPort)))) {

                usingThisPort = currentIdentifier;

                System.out.println("::::Identification du port::::");
                System.out.println("Port série trouvé : " + usingThisPort.getName());

                return usingThisPort;
            } else {
                usingThisPort = null;

            }
        }
        return usingThisPort;
    }

    /**
     * Méthode pour aquérir les permissions reliées au port série.
     */
    
    public static SerialPort takePortOwnership(CommPortIdentifier workingPort) throws PortInUseException, NullPointerException {

        SerialPort useablePort = (SerialPort) workingPort.open(applicationOwner, currentTimeOut);
        System.out.println("::::Acquisition du port::::");
        return useablePort;
    }

    /**
     * Méthode pour configurer le port serie Prends en parametre le SerialPort à
     * configurer Retourne le SerialPort configuré.
     */
    public static SerialPort configureCurrentSerialPort(SerialPort useablePort)
            throws UnsupportedCommOperationException {

        useablePort.setSerialPortParams(BAUD_RATE,
                SerialPort.DATABITS_8,
                SerialPort.STOPBITS_2,
                SerialPort.PARITY_EVEN);
        System.out.println("::::Configuration du port::::");
        return useablePort;
    }

    /**
     * Méthode qui configure le port pour l'envoie de caractères.
     */
    public static PrintStream outputStreamSetup(SerialPort configuredSerialPort) throws IOException {
        PrintStream output;
        output = new PrintStream(configuredSerialPort.getOutputStream(),true,"UTF-8");
        System.out.println("::::Acquisition du canal de communication::::");
        return output;
    }

    /*
     * Méthode qui récupère le message de l'utilisateur taper au clavier. 
     * Le programme quitte si l'utilisateur tape le mot 'sortie'.
     **/
    public static String writeMessage() {
        System.out.println("Entrez le message a envoyer (Entrez 'sortie' pour quitter): ");
        Scanner scan = new Scanner(System.in);
        String messageToSend = scan.nextLine();
        return messageToSend;
    }

    /**
     * Programme principal
     */
    public static void main(String[] args) {

        boolean connectionIsAlive = true;

        try {

            CommPortIdentifier workingPort = getProperSerialPort(portIdentifiers);
            SerialPort useablePort = takePortOwnership(workingPort);
            SerialPort configuredSerialPort = configureCurrentSerialPort(useablePort);
            PrintStream outputWriter = outputStreamSetup(configuredSerialPort);

            /**
             * TRANSACTIONS (unilatérale, dans ce cas)
             */
            System.out.println("::::Status : Prêt::::");

            while (connectionIsAlive) {

                String message = writeMessage();

                if ("sortie".equals(message)) {
                    outputWriter.println("SYSTEME : Déconnection du serveur.");
                    outputWriter.println("sortie");
                    System.out.print("Merci, au revoir.");
                    System.exit(0);
                }

                System.out.println("Le message envoyé : " + message);
                outputWriter.println(message);

            }

        } catch (NoSuchPortException e) {
            System.err.println("Une erreur est survenue lors de la détection des ports, veuillez réessayez.");
        } catch (NullPointerException NPE){
            System.out.println("Une erreur est survenue lors de la détection des ports, veuillez réessayez.");
            System.err.println("Vérifiez que le cable est bien branchez et relancez le programme.");
        } catch (PortInUseException e) {
            System.err.println("Le port ne peut garantir l'exclusivité à ce programme, veuillez relancer le programme.");
        } catch (UnsupportedCommOperationException UCOE) {
            System.out.println("Le port ne peut être configurer pour le moment, veuillez relancer le programme.");
        } catch (IOException IOE) {
            System.err.printf("Erreur lors de l'élaboration du protocole, veuillez relancer le programme.");
        } 
    }
}
